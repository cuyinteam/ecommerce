class Person:
    
    def __init__(self):
        self.name=input("Ingrese el nombre:")
        self.age=int(input("Ingrese la edad:"))

    def printInformation(self):
        print("Nombre:",self.name)
        print("Edad:",self.age)


class Profession(Person):

    def __init__(self):
        super().__init__()
        self.salary=float(input("Ingrese el sueldo:"))

    def imprimir(self):
        super().printInformation()
        print("Sueldo:",self.salary)

# bloque principal

employee1=Profession()
employee1.imprimir()
