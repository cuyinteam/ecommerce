class Person:
    #like constructor
    def __init__(self):
        self.name=input("Ingrese el nombre de la persona")
        self.age=int(input("Ingresa la edad de la persona"))
        self.sex=input("Ingresa el sexo de la persona")
        #you can call the methods from constructor and this method will be invoque first in console
        self.printSex()     
        
    def printInformation(self):
        print("Nombre:",self.name)
        print("Edad:",self.age)
        
    def printSex(self):
        print("El sexo de la persona es",self.sex)
        
#main
person1=Person()
person1.printInformation()
